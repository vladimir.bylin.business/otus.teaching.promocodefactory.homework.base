﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected static ICollection<T> Data { get; set; }

        public InMemoryRepository(ICollection<T> data)
        {
            Data ??= data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public async Task<Guid> CreateAsync(T entity)
        {
            entity.Id = Guid.NewGuid();
            await Task.Run(() => Data.Add(entity));

            return entity.Id;
        }

        public async Task<bool> UpdateAsync(T entity)
        {
            var entityBeforeUpdate = Data.FirstOrDefault(e => e.Id == entity.Id);

            if (entityBeforeUpdate == default)
            {
                return false;
            }

            Data.Remove(entityBeforeUpdate);
            await Task.Run(() => Data.Add(entity));

            return true;
        }
        
        public async Task<bool> DeleteByIdAsync(Guid id)
        {
            var entity = Data.FirstOrDefault(e => e.Id == id);
            
            return await Task.FromResult(Data.Remove(entity));
        }
    }
}