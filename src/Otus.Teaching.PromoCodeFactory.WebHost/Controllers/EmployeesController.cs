﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <param name="createEmployee"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateEmployeeAsync(EmployeeCreateRequest createEmployee)
        {
            var employee = new Employee
            {
                FirstName = createEmployee.FirstName,
                LastName = createEmployee.LastName,
                Email = createEmployee.Email
            };
            if (createEmployee.RoleId.HasValue)
            {
                employee.Roles.Add(await _roleRepository.GetByIdAsync(createEmployee.RoleId.Value));
            }

            var employeeId = await _employeeRepository.CreateAsync(employee);
            
            return Ok(employeeId);
        }

        /// <summary>
        /// Обновить данные сотрудника
        /// </summary>
        /// <param name="employeeUpdateRequest"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> UpdateEmployeeAsync(EmployeeUpdateRequest employeeUpdateRequest)
        {
            var employee = new Employee
            {
                Id = employeeUpdateRequest.Id,
                FirstName = employeeUpdateRequest.FirstName,
                LastName = employeeUpdateRequest.LastName,
                Email = employeeUpdateRequest.Email
            };
            if (employeeUpdateRequest.RoleId.HasValue)
            {
                employee.Roles.Add(await _roleRepository.GetByIdAsync(employeeUpdateRequest.RoleId.Value));
            }

            var isUpdated = await _employeeRepository.UpdateAsync(employee);

            if (isUpdated == false)
            {
                return NotFound();
            }

            return Ok();
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteEmployeeAsync(Guid id)
        {
            var isDeleted = await _employeeRepository.DeleteByIdAsync(id);
            
            if (isDeleted == false)
            {
                return NotFound();
            }

            return Ok();
        }
    }
}